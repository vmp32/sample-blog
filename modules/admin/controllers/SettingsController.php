<?php
namespace app\modules\admin\controllers;

use app\modules\admin\AdminController;
use app\modules\admin\models\SiteSettings;
use pheme\settings\SettingsAction;

class SettingsController extends AdminController{
    public function actions(){
        return [
            'index' => [
                'class' => SettingsAction::class,
                'modelClass' => SiteSettings::class,
                'viewName' => 'edit'
            ],
        ];
    }
}