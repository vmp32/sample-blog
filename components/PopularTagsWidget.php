<?php

namespace app\components;


use app\models\Tag;
use Yii;
use yii\base\Widget;

/**
 * Виджет популярных тегов.
 *
 * Class PopularTagsWidget
 * @package app\components
 */
class PopularTagsWidget extends Widget
{
    /**
     * @var int количество выводимых элементов
     */
    public $count = null;

    /**
     * Ключ, для доступа к кешу виджета
     */
    const CACHE_KEY = 'tags-cloud';

    public function init()
    {
        parent::init();

        if($this->count == null)
            $this->count = Yii::$app->settings->get('SiteSettings.tagsInCloud');
    }


    public function run()
    {
        $content = Yii::$app->cache->get(self::CACHE_KEY);
        if($content)
            return $content;

        $tags = Tag::getPopularTags($this->count);
        $content = $this->render('tags-cloud', compact('tags'));
        Yii::$app->cache->set(self::CACHE_KEY, $content, 0);
        return $content;
    }


    /**
     * Метод сброса кеша через код.
     * После вызова метода, содержимое виджета будет перегенерировано один раз
     */
    public static function clearCache()
    {
        Yii::$app->cache->delete(self::CACHE_KEY);
    }
}