<?php
/**
 * Created by PhpStorm.
 * User: ruvam
 * Date: 12.12.2016
 * Time: 7:42
 */

namespace app\modules\admin\controllers;


use app\components\LastPublicationsWidget;
use app\components\PopularTagsWidget;
use app\models\Post;
use app\modules\admin\AdminController;
use Yii;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;


/**
 * Контроллер управления записями в блоге
 *
 * Class BlogController
 * @package app\modules\admin\controllers
 */
class BlogController extends AdminController
{


    /**
     * Отображение списка публикаций
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->title = 'список публикаций';
        $posts = Post::getFeed();

        $pages = new Pagination([
            'totalCount' => $posts->count(),
            'pageSize' => 20,
            'forcePageParam' => false,
            'pageSizeParam' => false
        ]);


        $posts = $posts
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', compact('posts', 'pages'));
    }

    /**
     * Создание новой записи в блоге
     * @return string
     */
    public function actionCreate()
    {
        $post = new Post();

        // Обработка ajax-валидации в форме
        if (Yii::$app->request->isAjax && $post->load($this->post)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($post);
        }

        if ($post->load($this->post, 'Post') && $post->save()) {
            $this->clearCache();
            $this->session()->setFlash('info', \Yii::t('blog', 'Post has been saved'));
            return $this->redirect(['edit', 'id' => $post->id]);
        } else if (!empty($this->post)) {
            $this->session()->setFlash('error', 'cannot save data');
        }
        return $this->render('create', compact('post'));
    }

    /**
     * Редактирование записи в блоге
     *
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionEdit()
    {
        $id = $this->get('id');
        $post = Post::findOne($id);
        if (!$post)
            throw new NotFoundHttpException(\Yii::t('blog', 'Post with ID={id} not found', ['id' => $id]));

        // Обработка ajax-валидации в форме
        if (Yii::$app->request->isAjax && $post->load($this->post)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($post);
        }

        if ($post->load($this->post) && $post->save()) {
            $this->clearCache();
            $this->session()->setFlash('info', \Yii::t('blog', 'Post has been saved'));
        } else if (!empty($this->post)) {
            $this->session()->setFlash('error', 'cannot save data');
        }
        return $this->render('edit', compact('post'));
    }

    /**
     * Удаление записи из блога
     *
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDelete()
    {
        $id = $this->get('id');
        $post = Post::findOne($id);
        if (!$post)
            throw new NotFoundHttpException(\Yii::t('blog', 'Post with ID={id} not found', ['id' => $id]));

        $post->delete();
        $this->session()->setFlash('info', \Yii::t('blog', 'Post has been deleted'));
        $this->clearCache();
        return $this->redirect('index');
    }


    /**
     * Принудительная очистка кеша у виджетов
     */
    private function clearCache(){
        LastPublicationsWidget::clearCache();
        PopularTagsWidget::clearCache();
    }

}