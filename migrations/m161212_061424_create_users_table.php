<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m161212_061424_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'username' => $this->string(128)->notNull(),
            'password' => $this->string(128)->notNull(),
            'access_token' => $this->string(128),
            'auth_key' => $this->string(128)
        ]);

        $this->createIndex('indx_user_name', 'users', 'username');
        $this->createIndex('indx_user_key', 'users', 'auth_key');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('indx_user_name', 'users');
        $this->dropIndex('indx_user_key', 'users');
        $this->dropTable('users');
    }
}
