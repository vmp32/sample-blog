<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tags` and junction table for posts.
 */
class m161211_204000_create_tags_and_junction_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        // Создание таблицы с тэгами
        $this->createTable('tags', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'updated_at' => $this->integer()->unsigned()->defaultValue(0),
            'created_at' => $this->integer()->unsigned()->defaultValue(0)
        ]);


        // Создание таблицы со связью тегов и постов
        $this->createTable('post_tag', [
            'post_id' => $this->integer()->notNull(),
            'tag_id' => $this->integer()->notNull(),
            'PRIMARY KEY(post_id, tag_id)'
        ]);

        $this->createIndex('indx_post_tag', 'post_tag', 'post_id');
        $this->createIndex('indx_tag_post', 'post_tag', 'tag_id');

        // Внешний ключ для связи промежуточной таблицы и таблицы с тегами
        $this->addForeignKey(
            'fk_post_tag_tags',   // Название внешнего ключа
            'post_tag',             // Таблица, для которой создается внешний ключ
            'tag_id',               // Идентификатор связи в исходной таблице
            'tags',                 // Таблица на которую будет ссылаться внешний ключь
            'id',                   // Поле, через которое идет связывание
            'CASCADE'               // Тип индекса для связи
        );

        // Внешний ключ для связи промежуточной таблицы и таблицы с постами
        $this->addForeignKey(
            'fk_post_tag_posts',    // Название внешнего ключа
            'post_tag',             // Таблица, для которой создается внешний ключ
            'post_id',              // Идентификатор связи в исходной таблице
            'posts',                // Таблица на которую будет ссылаться внешний ключь
            'id',                   // Поле, через которое идет связывание
            'CASCADE'               // Тип индекса для связи
        );


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // Удаление внешних ключей
        $this->dropForeignKey('fk_post_tag_tags', 'post_tag');
        $this->dropForeignKey('fk_post_tag_posts', 'post_tag');

        // удаление индексов
        $this->dropIndex('indx_post_tag', 'post_tag');
        $this->dropIndex('indx_tag_post', 'post_tag');

        // Удаляем таблицу со связью постов и тегов
        $this->dropTable('post_tag');

        // Удаляем таблицу с тегами
        $this->dropTable('tags');
    }
}
