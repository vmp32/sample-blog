<?php
/**
 * Created by PhpStorm.
 * User: ruvam
 * Date: 12.12.2016
 * Time: 6:48
 */

namespace app\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * Модель описывающая цитату в приложении
 *
 * Class Quote
 * @package app\models
 * @property int $id
 * @property string $content
 * @property string $author
 */
class Quote extends ActiveRecord
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['content', 'required'],
            [['content', 'author'], 'trim']
        ];
    }


    public static function tableName()
    {
        return "quotes";
    }


    /**
     * Получение случайной цитаты
     *
     * @return Quote|ActiveRecord
     */
    public static function getRandom()
    {
        return self::find()->orderBy('Rand()')->limit(1)->one();
    }

    /**
     * ПОдписи к полум модели
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('quotes', 'ID'),
            'content' => Yii::t('quotes', 'Content'),
            'author' => Yii::t('quotes', 'Author'),
        ];
    }



}