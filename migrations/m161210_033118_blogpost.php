<?php

use yii\db\Migration;
use yii\db\Schema;

class m161210_033118_blogpost extends Migration
{
    const tableName = 'posts';
    public function up()
    {
        $this->createTable(self::tableName, [
            'id' => Schema::TYPE_PK,                    // Идентификатор
            'slug' => $this->string()->unique(),        // Префикс для адреса статьи
            'title' => Schema::TYPE_STRING.' NOT NULL', // Заголовок публикации
            'content' => Schema::TYPE_TEXT.' NOT NULL', // Содержимое публикации
            'image' => Schema::TYPE_STRING,             // Картинка, если есть
            'summary' => Schema::TYPE_TEXT,             // Краткое описание публикации, для ленты
            'description' => Schema::TYPE_STRING,       // Описание публикации, для правого блока и meta-тега
            'keywords' => Schema::TYPE_STRING,          // Ключевики, для мета-0тега

            'updated_at' => $this->integer()->unsigned()->defaultValue(0),     // Время последнего обновления
            'created_at' => $this->integer()->unsigned()->defaultValue(0),     // Время создания
        ]);
    }

    public function down()
    {
        $this->dropTable(self::tableName);

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
