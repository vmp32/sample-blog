<?php

namespace app\materialize;

/**
 * Переопределенный виджет ActiveForm для работы с materialize-css
 *
 * Class ActiveForm
 * @package app\materialize
 */
class ActiveForm extends \yii\widgets\ActiveForm
{

    public $fieldClass = ActiveField::class;

    public $options = ['role' => 'form'];

    public $layout = 'default';
}