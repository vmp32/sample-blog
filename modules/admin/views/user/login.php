<?php

/* @var $this yii\web\View */
/* @var $form app\materialize\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use app\materialize\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>


<h1>&nbsp;</h1>

<div class="row center-align">
    <div class="site-login card col m6 s12 offset-m3">
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <div class="card-content">
            <div class="card-title">
                Login
            </div>

            <div class="row">


                <div class="col s12">

                    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                </div>
                <div class="col s12">
                    <?= $form->field($model, 'password')->passwordInput() ?>
                </div>


                <div class="col s12">
                    <p>Запомнить?</p>
                    <?= $form->field($model, 'rememberMe')->switcher() ?>
                </div>
            </div>
            <div class="car-action">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>