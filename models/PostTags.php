<?php
/**
 * Created by PhpStorm.
 * User: ruvam
 * Date: 12.12.2016
 * Time: 0:24
 */

namespace app\models;


use yii\db\ActiveRecord;

/**
 * Модель описывающая таблицу связи между публикациями и тегами.
 * Используется для прямого доступа, при переопределении тегов у публикации
 * @see Post::updateTags()
 *
 * Class PostTags
 * @package app\models
 *
 * @property int post_id
 * @property int tag_id
 */
class PostTags extends ActiveRecord
{
    public static function tableName()
    {
        return 'post_tag';
    }

}