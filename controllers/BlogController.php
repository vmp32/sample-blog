<?php
/**
 * Created by PhpStorm.
 * User: ruvam
 * Date: 10.12.2016
 * Time: 0:35
 */

namespace app\controllers;


use app\common\Controller;
use app\models\Post;
use app\models\Tag;
use Yii;
use yii\data\Pagination;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;

/**
 * Публичный контроллер записей блога. Отвечает за демонстрацию
 * списков публикаций и отдельных записей
 *
 * Class BlogController
 * @package app\controllers
 */
class BlogController extends Controller
{
    /**
     * Отображение списка публикаций упорядоченных по дате добавления
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->title = Yii::t('blog', 'list of publications');
        $posts = Post::getFeed();

        $pages = new Pagination($this->paginationOptions([
            'totalCount' => $posts->count()
        ]));


        $posts = $posts
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', compact('posts', 'pages'));
    }

    /**
     * Просмотр отдельной публикации
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView()
    {
        $id = $this->get('id');
        $slug = $this->get('slug');
        $post = empty($slug) ? Post::findOne($id) : Post::findBySlug($slug);

        if (empty($post))
            throw new NotFoundHttpException(\Yii::t('blog', 'Post with ID={id} not found', ['id' => $id]));

        return $this->render('view', compact('post'));
    }


    /**
     * Вывод списка публикаций по тегу
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionTag()
    {
        $tagName = $this->get('tag');
        $tag = Tag::findOne(['name' => $tagName]);
        if(empty($tag))
            throw new NotFoundHttpException(\Yii::t('blog', 'now publications for tag {tag}', ['tag' => $tagName]));

        $query = $tag->getPosts()->with('tags')->orderBy(['updated_at' => SORT_DESC]);


        $pages = new Pagination($this->paginationOptions([
            'totalCount' => $query->count()
        ]));


        $posts = $query
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', compact('posts', 'pages'));
    }


    /**
     * Получение общих настроек пагинации для контроллера
     *
     * @param array $additional
     * @return array
     */
    protected function paginationOptions($additional = [])
    {
        return array_merge([
            // Количество элементов берется из настроек
            'pageSize' => Yii::$app->settings->get('SiteSettings.postsPerPage'),
            'forcePageParam' => false,
            'pageSizeParam' => false
        ], $additional);
    }



}