<?php
/** @var \app\models\Post[] $posts */
use app\components\LastPublicationsWidget;
use app\components\PopularTagsWidget;
use app\components\RandomQuoteWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use app\materialize\LinkPager;

?>
<div class="row">
    <div class="col hide-on-med-and-down l3 right">
        <?= LastPublicationsWidget::widget(['count' => 6]); ?>
        <p></p>
        <?= RandomQuoteWidget::widget(); ?>

        <p>&nbsp;</p>
        <?= PopularTagsWidget::widget([]); ?>
    </div>
    <div class="col l9 m12">
        <?php foreach ($posts as $post): ?>
            <div class="card post">

                <?php if (!empty($post->image)): ?>
                    <div class="card-image valign-wrapper">

                        <?= Html::img($post->image); ?>

                        <span class="card-title">
                            <a href="<?= Url::to(array_merge(['blog/view'], $post->navParams)) ?>">
                                <?= $post->title; ?>
                            </a>
                            <small>Опубликовано <a href="#">admin</a> <?= date('H:i', $post->updated_at); ?></small>
                        </span>
                    </div>
                <?php endif; ?>
                <div class="card-content">
                    <?php if (empty($post->image)): ?>
                        <span class="card-title">
                            <a href="<?= Url::to(array_merge(['blog/view'], $post->navParams)) ?>">
                                <?= $post->title; ?>
                            </a>
                    <small>Опубликовано <a href="#">admin</a> <?= date('H:i', $post->updated_at); ?></small>
                </span>
                    <?php endif; ?>
                    <?= $post->intro; ?>
                </div>
                <div class="card-action">
                    <span>Тэги: </span>
                    <?php foreach ($post->tags as $tag): ?>
                        <small>
                            <a href="<?= Url::to(['blog/tag', 'tag' => $tag->name]); ?>" class="waves-effect text-deep-purple text-darken-1">
                                <?= $tag->name; ?>
                            </a>
                        </small>
                    <?php endforeach; ?>

                    <a href="<?= Url::to(array_merge(['blog/view'], $post->navParams)) ?>"
                       class="right purple-text waves-effect">
                        <?= \Yii::t('blog', 'More info'); ?>
                    </a>
                    <div class="clearfix"></div>
                </div>
            </div>
        <?php endforeach; ?>


        <div class="center">
            <?= LinkPager::widget(['pagination' => $pages]); ?>
        </div>
    </div>

</div>