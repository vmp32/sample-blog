<?php

namespace app\models;


use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Модель описывающая тег в приложении
 *
 * Class Tag
 * @package app\models
 *
 * @property int $id
 * @property string $name
 * @property Post[] $posts
 */
class Tag extends ActiveRecord
{

    public function rules()
    {
        return [
            ['name', 'required']
        ];
    }



    public static function tableName()
    {
        return "tags";
    }


    /**
     * Доступ до записей, которым назначен данный тэг
     *
     * @return Post[]|ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::class, ['id' => 'post_id'])->viaTable('post_tag', ['tag_id' => 'id']);
    }


    /**
     * Helper-метод, позволяющий либо загрузить существующий,
     * либо создать новый тэг
     *
     * @param null $name
     * @return Tag|null|static
     */
    public static function findOrCreate($name = null)
    {
        if (empty($name))
            return null;

        $tag = Tag::findOne(['name' => $name]);
        if (!$tag) {
            $tag = new Tag(['name' => $name]);
            $tag->save();
        }
        return $tag;

    }

    /**
     * Обработчики событий
     *  - Обрабатываем события перед сохранением для установки времени создания и последней модификации.
     *
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at']
                ]
            ]
        ];
    }


    /**
     * Метод интеграции с виджетом. Получаем список самых популярных тегов,
     * упорядоченный по количеству публикаций использующих данный тэг
     *
     * @param int $count
     * @return Tag[]|ActiveRecord[]
     */
    public static function getPopularTags($count = 10)
    {
        $tagsTable = self::tableName();
        $linkedTable = PostTags::tableName();
        return self::find()
            ->select([$tagsTable.'.*', 'Count('.$linkedTable.'.tag_id) as tagCount'])
            ->join('LEFT JOIN', $linkedTable, $tagsTable.'.id='.$linkedTable.'.tag_id')
            ->groupBy($tagsTable.'.id')
            ->orderBy(['tagCount' => SORT_DESC])
            ->asArray()
            ->limit($count)
            ->all();
    }


}