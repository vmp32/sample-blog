<?php

/**
 * @var \yii\web\View $this
 * @var \app\models\Post $post
 */
$mess = \Yii::$app->getSession()->getFlash('post_save');
?>
<?= $this->render('@app/views/_common/messages'); ?>

<h2>Редактирование <?= $post->title; ?></h2>

<div class="card">
    <div class="card-content">
        <?= $this->render('_editForm', ['post' => $post]); ?>
    </div>
</div>
