<?php
/**
 * Created by PhpStorm.
 * User: ruvam
 * Date: 12.12.2016
 * Time: 6:48
 */

namespace app\components;


use app\models\Quote;
use Yii;
use yii\base\Widget;

/**
 * Вывод случайной цитаты
 *
 * Class RandomQuoteWidget
 * @package app\components
 */
class RandomQuoteWidget extends Widget
{
    public function run()
    {
        return $this->render('random-quote', ['quote' => Quote::getRandom()]);
    }
}