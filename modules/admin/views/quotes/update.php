<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Quote */

$this->title = Yii::t('quotes', 'Update quote');
$this->params['breadcrumbs'][] = ['label' => Yii::t('quotes', 'Quotes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('quotes', 'Update');
?>
<div class="quote-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="card">
        <div class="card-content">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
