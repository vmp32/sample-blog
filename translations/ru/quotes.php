<?php
return [
    'Quotes' => 'Цитаты',
    'Create Quote' => 'Добавить цитату',
    'Update quote' => 'Редактировать цитату',
    'Create' => 'Добавить',
    'Update' => 'Сохранить',
    'Content' => 'Цитата',
    'Author' => 'Автор'
];