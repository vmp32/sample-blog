<?php
/**
 * Created by PhpStorm.
 * User: ruvam
 * Date: 12.12.2016
 * Time: 7:42
 */

namespace app\modules\admin;


use app\common\Controller;
use yii\filters\AccessControl;

class AdminController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true
                    ]
                ]
            ]
        ];
    }
}