(function () {
    $(document).ready(function () {
        $('.parallax').parallax();
        $('.fixed-on-scroll').pushpin({
            top: $('.parallax').first().height() + 0,
        });

        $('[data-hide]').click(function (e) {
            e.preventDefault();
            $closed = $(this).data('hide');
            $('#' + $closed).fadeOut(300);
        });

        $('.chip-input').each(function () {

            var $input = $(this);
            var source = [];
            var $container = $input.find('[data-role=source]');
            var line = $container.val();
            if (line) {
                line.split(', ').forEach(function (elem) {
                    source.push({'tag': elem});
                });
            }
            var chip = $input.find('.chips');

            var updateValue = function () {
                var str = [];

                $(chip.data().chips).each(function(){
                    str.push(this.tag);
                });
                $container.val(str.join(', '));
                console.log($container.val());
            };


            chip.on('chip.add', updateValue)
                .on('chip.delete', updateValue)
                .material_chip({
                    data: source
                })
        });
    });

})();