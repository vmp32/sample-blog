<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('quotes', 'Quotes');
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="card">
    <div class="card-content">


        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'tableOptions' => ['class' => 'striped highlight'],
            'pager' => ['class' => \app\materialize\LinkPager::class],
            'columns' => [

                'id',
                'content:html',
                'author:html',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['style' => 'min-width: 100px;'],
                    'buttons' => [
                        'update' => function ($url, $quote) {
                            return Html::a('<i class="material-icons">create</i>', $url);
                        },
                        'view' => function ($url, $quote) {
                            return '';
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<i class="material-icons">delete_forever</i>', $url, [
                                'title' => Yii::t('yii', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'post',
                                'data-pjax' => '1',
                            ]);
                        },
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>
<div class="fixed-action-btn">
    <a class="btn-floating btn-large red" href="<?= Url::to(['/admin/quotes/create']); ?>">
        <i class="large material-icons">add</i>
    </a>
</div>
