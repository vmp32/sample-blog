<?php

use yii\db\Migration;

/**
 * Handles the creation of table `quotes`.
 */
class m161212_034409_create_quotes_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('quotes', [
            'id' => $this->primaryKey(),
            'content' => $this->text(),
            'author' => $this->string(128)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('quotes');
    }
}
