<?php
/** @var \app\models\Post[] $posts */
use yii\helpers\Url;

if ($posts): ?>
    <h5>Последние публикации</h5>
    <?php foreach ($posts as $post):
        ?>
        <a href="<?= Url::to(array_merge(['blog/view'], $post->navParams)) ?>" class="additional-link">
            <div class="card">
                <strong><?= $post->title; ?></strong>
                <div class="card-content">
                    <p><?= $post->smallIntro; ?></p>
                    <small><?php date('d.m.Y h:i', $post->updated_at); ?></small>
                </div>
            </div>
        </a>
        <?php
    endforeach;
    endif;
?>