<?php
/** @var \app\models\Quote $quote */
if(!empty($quote)):
?>
    <blockquote data-role="random-quote">
        <?= $quote->content; ?>
        <p class="right-align">
            <small><?= $quote->author; ?></small>
        </p>
    </blockquote>
<?php endif; ?>
