<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\SiteAsset;
use yii\helpers\Html;
use yii\helpers\Url;


SiteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="parallax-container">
    <div class="parallax">
        <?= Html::img('@web/images/template/main.jpg', ['alt' => 'Уголок копирайтера']); ?>
    </div>
    <div class="background valign-wrapper">
        <div class="cont">
            <img  class="circle responsive-img z-depth-2" src="http://placehold.it/96x96">
            <h1><?= Yii::$app->settings->get('SiteSettings.siteName'); ?></h1>
            <h4><?= Yii::$app->settings->get('SiteSettings.siteSlogan'); ?></h4>
        </div>
    </div>
</div>
<div class="navbar">
    <nav class="fixed-on-scroll">
        <div class="nav-wrapper purple accent-3">
            <div class="container">
                <a href="<?= Url::home(); ?>" class="brand-logo"><?= Yii::$app->settings->get('SiteSettings.brandName'); ?></a>
                <ul class="right">
                </ul>
            </div>
        </div>
    </nav>
</div>
<div class="container main-content-area">
        <?= $content ?>
</div>

<footer class="page-footer purple lighten-4">
    <div class="container row">
        <p class="col s6">&copy; <?= Yii::$app->settings->get('SiteSettings.copyrightName'); ?> <?= date('Y') ?></p>
        <p class="col s6"><?= Yii::powered() ?></p>
    </div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
