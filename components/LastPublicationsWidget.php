<?php
/**
 * Created by PhpStorm.
 * User: ruvam
 * Date: 12.12.2016
 * Time: 5:57
 */

namespace app\components;
use app\models\Post;
use Yii;
use yii\base\Widget;

/**
 * Виджет последних публикаций
 *
 * Class LastPublicationsWidget
 * @package app\components
 */
class LastPublicationsWidget extends Widget
{

    /**
     * Количество выводимых элементов
     *
     * @var int
     */
    public $count = 5;

    const CACHE_KEY = 'last-publications';



    public function run()
    {
        $content = Yii::$app->cache->get(self::CACHE_KEY);
        if($content)
            return $content;

        $posts = Post::getFeed()->limit($this->count)->all();
        $content = $this->render('last-publications', compact('posts'));
        Yii::$app->cache->set(self::CACHE_KEY, $content, 0);
        return $content;
    }


    /**
     * Метод сброса кеша через код.
     * После вызова метода, содержимое виджета будет перегенерировано один раз
     */
    public static function clearCache()
    {
        Yii::$app->cache->delete(self::CACHE_KEY);
    }
}