<?php

use yii\helpers\Url;

if ($tags): ?>
    <h5>
        Популярные теги
    </h5>
    <?php foreach ($tags as $tag):
        ?>

        <a href="<?= Url::to(['blog/tag', 'tag' => $tag['name']]); ?>" class="chip">
            <?= $tag['name']; ?>
        </a>
        <?php
    endforeach;
endif;

