<?php

namespace app\modules\admin;

use app\modules\admin\models\User;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        User::ensureCreate();

        // custom initialization code goes here
    }
}
