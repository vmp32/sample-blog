<?php

/** @var \app\models\Post[] $posts */
use app\materialize\LinkPager;
use yii\helpers\Url;

?>
<h2>Список публикаций</h2>

<?= $this->render('@app/views/_common/messages'); ?>

<div class="card">
    <div class="card-content">
        <table class="striped highlight">
            <thead>
            <tr>
                <th><?= Yii::t('blog', 'ID'); ?></th>
                <th><?= Yii::t('blog', 'Title'); ?></th>
                <th><?= Yii::t('blog', 'Last modified'); ?></th>
                <th><?= Yii::t('blog', 'Actions'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($posts as $post): ?>
                <tr>
                    <td><?= $post->id; ?></td>
                    <td><?= $post->title; ?></td>
                    <td><?= date('d.m.Y H:i', $post->updated_at); ?></td>
                    <td>
                        <a href="<?= Url::to(['/admin/blog/edit', 'id' => $post->id]); ?>">
                            <i class="material-icons">create</i>
                        </a>
                        <a data-role="delete" data-hint="<?= $post->title; ?>" href="<?= Url::to(['/admin/blog/delete', 'id' => $post->id]); ?>">
                            <i class="material-icons">delete_forever</i>
                        </a>

                        <a href="<?= Url::to(['/blog/view', 'id' => $post->id]); ?>">
                            <i class="material-icons">reply</i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="card-content">
        <div class="center">
            <?= LinkPager::widget(['pagination' => $pages]); ?>
        </div>
    </div>
</div>

<div class="fixed-action-btn">
    <a class="btn-floating btn-large red" href="<?= Url::to(['/admin/blog/create']); ?>">
        <i class="large material-icons">add</i>
    </a>
</div>