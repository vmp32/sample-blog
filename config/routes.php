<?php
/**
 * Файл с переопределениями маршрутов
 */
return [
    '/post/<id:\d+>' => 'blog/view',
    '/post/<slug:\S+>' => 'blog/view',
    '/page/<page:\d+>' => 'blog/index',
    '/tag/<tag:.+>' => 'blog/tag',
    '/admin' => '/admin/blog/index',
    '/admin/login' => '/admin/user/login',
    '/admin/logout' => '/admin/user/logout'
];