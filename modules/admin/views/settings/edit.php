<?php
use app\materialize\ActiveForm;
use yii\helpers\Html;

/** @var \app\modules\admin\models\SiteSettings model */
?>
<h3>Настройки</h3>
<div class="card">
    <div class="card-content">
        <?php

        $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'siteName')->textInput() ?>
        <?= $form->field($model, 'siteSlogan')->textInput() ?>
        <?= $form->field($model, 'brandName')->textInput() ?>
        <?= $form->field($model, 'postsPerPage')->textInput() ?>
        <?= $form->field($model, 'copyrightName')->textInput() ?>
        <?= $form->field($model, 'tagsInCloud')->textInput() ?>

        <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn']) ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>