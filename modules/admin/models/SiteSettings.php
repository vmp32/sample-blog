<?php

namespace app\modules\admin\models;


use yii\base\Model;

class SiteSettings extends Model
{
    public
        $siteName,
        $siteSlogan,
        $postsPerPage = 10,
        $copyrightName = 'julia',
        $tagsInCloud = 10,
        $brandName = 'corner';


    public function rules()
    {
        return [
            [
                ['siteName', 'siteSlogan', 'postsPerPage', 'copyrightName', 'tagsInCloud', 'brandName'],
                'required'
            ],
            [
                ['postsPerPage', 'tagsInCloud'], 'integer'
            ]
        ];
    }

    public function attributes()
    {
        return ['siteName', 'siteSlogan', 'postsPerPage', 'copyrightName', 'tagsInCloud', 'brandName'];
    }
}