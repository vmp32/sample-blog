<?php
namespace app\models;


use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Модель публикации в блоге
 *
 * Class Post
 * @package app\models
 * @property int $id идентификатор публикации
 * @property string $slug uri для seo
 * @property string $title заголовок публикации
 * @property string $content содержимое публикации
 * @property string $image основная картинка поста
 * @property string $summary краткое содержание публикации, для вывода в ленте
 * @property string $description описание публикации, для вывода в meta-тэгах
 * @property string $keywords ключевые слова публикации, для вывода в meta-тэгах
 * @property int $created_at служебное поле = время создания публикации
 * @property int $updated_at служебное поле = время последнего изменения публикации
 * @property Tag[] $tags автоматическое поле - список тегов назначенных публикации
 *
 *
 * @see Post::getIntro()
 * @property string intro - генерируемое через аксессор поле, краткого содержания.
 *
 *
 * @see Post::getSmallIntro()
 * @property string smallIntro - генерируемое через аксессор поле, соедржания для списка последних публикаций
 *
 */
class Post extends ActiveRecord
{

    /**
     * @var Tag[] - служебное поле, для хранения переназначенных тегов
     */
    protected $_tags = [];


    /**
     * Helper для поиска публикации по URI, по аналогии с findOne($id)
     *
     * @param $slug
     * @return static
     */
    public static function findBySlug($slug)
    {
        return self::findOne(['slug' => $slug]);
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            [[
                'title',
                'content',
                'summary',
                'description',
                'keywords',
                'image',
                'slug',
                'tagsLine'
            ], 'trim'],
            [['title', 'content'], 'required'],
            ['slug', 'checkSlug'],
        ];
    }

    /**
     * Проверка на потоврение slug (URI). Используется при ajax запросе
     *
     * @param $attribute
     * @param $params
     */
    public function checkSlug($attribute, $params)
    {
        $post = self::findBySLug($this->slug);
        if (!empty($post) && $post->id != $this->id)
            $this->addError('slug', \Yii::t('blog', 'slug {slug} is already used by {page}',
                ['slug' => $post->slug, 'page' => $post->title]
            ));
    }


    /**
     * Аксессор для получения краткого содержания статьи
     * @return string
     */
    public function getIntro()
    {
        if (!empty($this->summary))
            return $this->summary;

        $content = mb_substr(strip_tags($this->content, '<br><p>'), 0, 1024);
        return mb_strlen($content) == 1024 ? mb_substr($content, 0, mb_strrpos($content, ' ')) . '...' : $content;
    }


    /**
     * Аксессор для получения описания статьи, при выводе в боковой панели
     *
     * @return string
     */
    public function getSmallIntro()
    {
        if (!empty($this->description))
            return $this->description;

        $content = mb_substr(strip_tags($this->content, '<br><p>'), 0, 255);
        return mb_strlen($content) == 255 ? mb_substr($content, 0, mb_strrpos($content, ' ')) . '...' : $content;
    }


    public static function tableName()
    {
        return "posts";
    }


    /**
     * аксессор, для установки переопределенных тегов
     * @param array $_tags
     */
    public function setTags($_tags = [])
    {
        $this->tags = $_tags;
    }

    /**
     * Получение списка связанных с публикацией тегов
     * @return Tag[]|ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::class, ['id' => 'tag_id'])->viaTable('post_tag', ['post_id' => 'id']);
    }


    /**
     * Хук перед сохранением, если пришел пустой slug, тогда устанавливаем в null, чтобы не
     * возникало конфликтов с остальными записями.
     *
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (empty($this->slug))
            $this->slug = null;

        return parent::beforeSave($insert);
    }

    /**
     * Хук после сохранения объекта, если теги были переопределены,
     * тогда производится обновление связей
     *
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        // Если менялся набор тегов
        if (!empty($this->_tags))
            $this->updateTags(); // Тогда обновляем теги

        parent::afterSave($insert, $changedAttributes);
    }


    /**
     * Внутренныяя функция обновления связей с тегами
     */
    protected function updateTags()
    {
        $vals = [];
        PostTags::deleteAll(['post_id' => $this->id]);
        foreach ($this->_tags as $tag) {
            $vals[$tag->id] = [$this->id, $tag->id];
        }

        $this->getDb()
            ->createCommand()
            ->batchInsert(PostTags::tableName(), ['post_id', 'tag_id'], $vals)->execute();

    }


    /**
     * Функция для интеграции с интерфейсом приложения.
     * Получаем список тегов в строке, разделенный запятыми.
     *
     * @return string
     */
    public function getTagsLine()
    {
        $line = [];
        foreach ($this->tags as $tag) {
            $line[] = $tag->name;
        }
        return implode(', ', $line);
    }


    /**
     * Функция интеграции с интерфейсом прилоежния.
     * Восстанавливаем список тегов из строки. При необходимости создает новые.
     *
     * @param string $tags
     */
    public function setTagsLine($tags = '')
    {
        if (empty($tags))
            return;
        $tagsName = explode(',', $tags);
        foreach ($tagsName as $tagName) {
            $this->_tags[] = Tag::findOrCreate(trim($tagName));
        }
    }


    /**
     * Получение объекта запроса описывающий ленту публикаций сайта
     *
     * @return ActiveQuery
     */
    public static function getFeed()
    {
        return self::find()->orderBy(['updated_at' => SORT_DESC])->with('tags');
    }


    /**
     * Обработчики событий
     *  - Обрабатываем события перед сохранением для установки времени создания и последней модификации.
     *
     *
     * @return array
     */
    public function behaviors()
    {
        parent::behaviors();
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at']
                ]
            ]
        ];
    }


    /**
     * Метод для интеграции с интерфейсом - получение пераметров навигации,
     * в зависимости от наличия slug в объекте публикации.
     *
     * @return array
     */
    public function getNavParams()
    {
        return empty($this->slug) ? ['id' => $this->id] : ['slug' => $this->slug];
    }
}