<?php
/**
 * Вывод разовых сообщений из сессии
 */

$session = \Yii::$app->getSession();
$info = $session->getFlash('info');
$error = $session->getFlash('error');


if (!empty($info)): ?>
    <div class="card green darken-3" id="info-box">
        <div class="card-content white-text">
            <?= $info; ?>
            <a href="#" class="material-icons text-white right" data-hide="info-box">close</a>
        </div>
    </div>
<?php endif; ?>


<?php if (!empty($error)): ?>
    <div class="card red darken-3" id="error-box">
        <div class="card-content white-text">
            <?= $error; ?>
            <a href="#" class="material-icons text-white right" data-hide="error-box">close</a>
        </div>
    </div>
<?php endif; ?>