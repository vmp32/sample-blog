# Модели приложения

- Post - модель публикации
    > Методы
    - ```static findBySlug($slug)``` - Helper для поиска публикации по URI, по аналогии с findOne($id)
    - ```checkSlug``` - Валидатор. Проверка на потоврение slug (URI). Используется при ajax запросе
    - ```getIntro()``` - Аксессор для получения краткого содержания статьи
    - ```getSmallIntro()``` - Аксессор для получения описания статьи, при выводе в боковой панели
    - ```setTags($_tags = [])``` - Аксессор, для установки переопределенных тегов
    - ```getTags()``` - Получение списка связанных с публикацией тегов
    - ```getTagsLine()``` - Получаем список тегов в строке, разделенный запятыми.
    - ```setTagsLine($tags = '')``` - Восстанавливаем список тегов из строки. При необходимости создает новые.
    - ```getFeed()``` - Получение объекта запроса описывающий ленту публикаций сайта
    - ```getNavParams()``` - получение пераметров навигации, в зависимости от наличия slug в объекте публикации.
    
- PostTag - таблица для связи тэгов и публикаций

- Quote - таблица для хранения случайных цитат
    > Методы
    - ```static getRandom()``` - Получение случайной цитаты
    
- Tag - таблица с тегами
    > Методы
    - ```static findOrCreate($name = null)``` - Helper-метод, позволяющий либо загрузить существующий, либо создать новый тэг
