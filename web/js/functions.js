(function(){
    $(document).ready(function(){
        var updateQuote = function(){
            $this = $(this);

            $.ajax({
                'url': '/quotes/new',
                'type': 'GET'
            }).success(function(res){
                $this.replaceWith(res);
                $('[data-role=random-quote]').click(updateQuote);
            });
        };
        $('[data-role=random-quote]').click(updateQuote);


        $('[data-role=delete]').click(function(e){
            var hint = $(this).data('hint');
            if(!confirm('Действительно удалить "' + hint+ '"?')){
                e.preventDefault();
            }
        })
    });
})();