<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=julia-blog',
    'username' => 'bloguser',
    'password' => 'bloguserpassword',
    'charset' => 'utf8',
    'enableSchemaCache' => YII_DEBUG == false,
];
