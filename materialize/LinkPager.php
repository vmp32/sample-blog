<?php
/**
 * Created by PhpStorm.
 * User: ruvam
 * Date: 12.12.2016
 * Time: 5:24
 */

namespace app\materialize;

/**
 * Переопределенный виджет пагинации, для работы с materialize-css
 *
 * Class LinkPager
 * @package app\materialize
 */
class LinkPager extends \yii\widgets\LinkPager
{
    public $prevPageLabel = '<i class="material-icons">chevron_left</i>';
    public $nextPageLabel = '<i class="material-icons">chevron_right</i>';
    public $activePageCssClass = 'active z-depth-2';
    public $disabledPageCssClass = 'disabled';
    public $linkOptions = ['class' => 'waves-effect'];

}