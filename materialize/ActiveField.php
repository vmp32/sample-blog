<?php

namespace app\materialize;

use yii\helpers\Html;

/**
 * Переопределенный виджет ActiveField для работы с materialize-css
 *
 * Class ActiveField
 * @package app\materialize
 */
class ActiveField extends \yii\widgets\ActiveField
{
    public $inputOptions = [
        'class' => 'validate'
    ];

    public $options = ['class' => 'input-field'];

    public $template = '{label}{input}{hint}{error}';

    public function textarea($options = [])
    {
        $options = array_merge($this->inputOptions, $options, ['class' => 'materialize-textarea']);
        $this->adjustLabelFor($options);
        $this->parts['{input}'] = Html::activeTextarea($this->model, $this->attribute, $options);
        return $this;
    }

    public function switcher($options = [], $enclosedByLabel = true)
    {
        $name = isset($options['name']) ? $options['name'] : Html::getInputName($this->model, $this->attribute);
        $value = Html::getAttributeValue($this->model, $this->attribute);
        $input = Html::checkbox($name, $value == 1?'checked':'unchecked', $options);

        $this->template = '<div class="switch"><label>Off'.$input.'<span class="lever"></span>On</label></div>';
        $this->options = [];

        return $this;
    }


}