<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\SiteAsset;
use yii\helpers\Html;
use yii\helpers\Url;


SiteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="navbar">
    <nav class="fixed-on-scroll">
        <div class="nav-wrapper purple accent-3">
            <div class="container">
                <a href="<?= Url::to(['/admin/blog/index']); ?>" class="brand-logo">Панель управления</a>
                <ul class="right">
                    <li>
                        <a href="<?= Url::to(['/admin/settings']); ?>" class="waves-effect waves-light">
                            Настройки
                        </a>
                    </li>
                    <li>
                        <a href="<?= Url::to(['/admin/blog']); ?>" class="waves-effect waves-light">
                            Управление публикациями
                        </a>
                    </li>
                    <li>
                        <a href="<?= Url::to(['/admin/quotes']); ?>" class="waves-effect waves-light">
                            Редактирование цитат
                        </a>
                    </li>

                    <li>
                        <a href="<?= Url::to(['/admin/logout']); ?>" class="waves-effect waves-light">
                            выход
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<div class="container admin-content-area">
    <?= $content ?>
</div>

<footer class="page-footer purple lighten-4">
    <div class="container row">
        <p class="col s6">&copy; Julia <?= date('Y') ?></p>
        <p class="col s6"><?= Yii::powered() ?></p>
    </div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
