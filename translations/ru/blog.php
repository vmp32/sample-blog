<?php
return [
    'Post has been saved' => 'Публикация была успешно сохранена',
    'Post with ID={id} not found' => 'Публикация с ID = {id} не найдена!',
    'More info' => 'Подробнее...',
    'slug {slug} is already used by {page}' => 'Адрес {slug} уже используется страницей "{page}"',
    'now publications for tag {tag}' => 'Нет публикаций с тегом {tag}',
    'list of publications' => 'Список публикаций'
];