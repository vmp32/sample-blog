<?php
use app\materialize\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use mihaildev\elfinder\InputFile;

/** @var \app\models\Post $post */
?>
<div>
    <div class="row">
        <ul class="tabs">
            <li class="tab col s4"><a class="active" href="#main-content">Содержимое</a></li>
            <li class="tab col s4"><a href="#seo">SEO</a></li>
            <li class="tab col s4"><a href="#feed">Данные для ленты</a></li>
        </ul>
    </div>
    <?php $form = ActiveForm::begin(['id' => 'contact-form','enableAjaxValidation' => true]); ?>


    <div id="main-content">
        <p>
            <?= $form->field($post, 'title')->textInput(); ?>
        </p>
        <p>
            <?= $form->field($post, 'slug')->textInput(); ?>
        </p>
        <p>
            <?=
            $form->field($post, 'content', ['labelOptions' => ['style' => 'display: none;'],
                'options' => ['style' => 'z-index: 9999999;']])
                ->widget(CKEditor::className(), [
                    'class' => 'main-post-editor',
                    'editorOptions' => ElFinder::ckeditorOptions(['elfinder', 'style' => 'z-index: 9999999'],
                        [
                            'preset' => 'full',
                            //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                            'inline' => false, //по умолчанию false
                        ]),

                ]);
            ?>
        </p>
        <div class="chip-input">
            <?= $form->field($post, 'tagsLine', ['labelOptions' => ['style' => 'display:none;']])->hiddenInput(['data-role' => 'source']); ?>
            <div class="chips">Теги</div>
        </div>
    </div>
    <div id="seo">
        <p>
            Дополнительные параметры для поисковых систем
        </p>
        <p>
            <?= $form->field($post, 'description')->textarea(); ?>
        </p>
        <p>
            <?= $form->field($post, 'keywords')->textarea(); ?>
        </p>
    </div>

    <div id="feed">
        <?= $form->field($post, 'image')->widget(InputFile::className(), [
            'language' => 'ru',
            'filter' => 'image',
            'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
            'options' => ['class' => 'input-field'],
            'buttonOptions' => ['class' => 'btn blue darken-3 text-white'],
            'multiple' => false
        ]);
        ?>
        <p>
            <?= $form->field($post, 'summary')->textarea(); ?>
        </p>
    </div>

    <p>
        <?= \yii\helpers\Html::submitButton('Сохранить', ['class' => 'btn']); ?>
    </p>

    <?php ActiveForm::end(); ?>
</div>
