<?php
namespace app\assets;

use app\materialize\MaterializeAsset;
use yii\web\AssetBundle;

/**
 * Стили и скрипты сайта
 *
 * Class SiteAsset
 * @package app\assets
 */
class SiteAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
        'js/functions.js'
    ];
    public $depends = [
        MaterializeAsset::class
    ];
}
