<?php

namespace app\modules\admin\models;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;


/**
 * Class User
 * @package app\models
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $access_token
 * @property string $auth_key
 */
class User extends ActiveRecord implements IdentityInterface
{

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    public static function ensureCreate()
    {
        if(!self::findOne(['username' => 'admin']))
        {
            $user = new User(['username' => 'admin']);
            $user->createPassword('admin');
            $user->save();
        }
    }

    protected function security()
    {
        return \Yii::$app->getSecurity();
    }

    public static function tableName()
    {
        return "users";
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    public function createPassword($password)
    {
        $this->password = $this->security()->generatePasswordHash($password);
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {

        $isAuthed = $this->security()->validatePassword($password, $this->password );
        if($isAuthed){
            $this->generateAuthKey();
            $this->save();
        }
        return $isAuthed;
    }

    public function generateAuthKey()
    {
        $this->auth_key = $this->security()->generateRandomString(128);
    }
}
