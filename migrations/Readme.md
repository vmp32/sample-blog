# Миграции приложения

- [m161210_033118_blogpost](./m161210_033118_blogpost.php) - создание таблицы с публикациями

- [m161211_204000_create_tags_and_junction_table](./m161211_204000_create_tags_and_junction_table.php) - создание таблицы с тегами и связей с таблицей публикации

- [m161212_034409_create_quotes_table](./m161212_034409_create_quotes_table.php) -  создание таблицы с цитатами

- [m161212_061424_create_users_table](./m161212_061424_create_users_table.php) -  создание таблицы с пользователями