<?php
namespace app\controllers;

use app\common\Controller;
use app\models\Quote;

class QuotesController extends Controller
{
    /**
     * Вывод случайной цитаты, если запрос иницирован ajax - выводится без основного шаблона
     *
     * @return string
     */
    public function actionNew()
    {
        return (\Yii::$app->request->isAjax)?
            $this->renderPartial('@app/components/views/random-quote', ['quote' => Quote::getRandom()]):
            $this->render('@app/components/views/random-quote', ['quote' => Quote::getRandom()]);
    }
}