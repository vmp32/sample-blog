<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use app\materialize\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login card">
    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
    <div class="card-content">
        <h1><?= Html::encode($this->title) ?></h1>

        <p>Please fill out the following fields to login:</p>

        <div class="row">


            <div class="col m6 s12">

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
            </div>
            <div class="col m6 s12">
                <?= $form->field($model, 'password')->passwordInput() ?>
            </div>


            <div class="col m6 s12">
                <?= $form->field($model, 'rememberMe')->switcher() ?>
            </div>
        </div>
        <div class="car-action">
            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
