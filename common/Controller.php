<?php
/**
 * Created by PhpStorm.
 * User: ruvam
 * Date: 10.12.2016
 * Time: 0:32
 */

namespace app\common;


/**
 * Class Controller
 * @package app\common
 * Базовый контроллер для публичной части приложения
 *
 * @property string $title
 */
class Controller extends \yii\web\Controller
{

    /**
     * Получение текущего заголовка страницы
     *
     * @return null|string
     */
    public function getTitle()
    {
        return $this->view->title;
    }

    /**
     * Установка текущего
     *
     * @param string $val
     */
    public function setTitle($val){
        $this->view->title = $val;
    }

    /**
     * Получение параметра GET-запроса
     *
     * @param null $name
     * @return array|mixed
     */
    protected function get($name = null)
    {
        return \Yii::$app->request->get($name);
    }


    /**
     * Получение параметра POST-запроса
     *
     * @param null $name
     * @return array|mixed
     */
    protected function post($name = null)
    {
        return \Yii::$app->request->post($name);
    }


    /**
     * Получение массива параметров GET-запроса
     *
     * @return array|mixed
     */
    protected function getGet()
    {
        return \Yii::$app->request->get();
    }


    /**
     * Получение массива параметров POST-запроса
     *
     * @return array|mixed
     */
    protected function getPost()
    {
        return \Yii::$app->request->post();
    }


    /**
     * Получение объекта текущей сессии пользователя
     *
     * @return mixed|\yii\web\Session
     */
    protected function session()
    {
        return \Yii::$app->session;
    }


}