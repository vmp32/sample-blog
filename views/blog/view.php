<?php
use app\components\LastPublicationsWidget;
use app\components\PopularTagsWidget;
use app\components\RandomQuoteWidget;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \app\models\Post $post */
?>
<div class="row">
    <div class="col hide-on-med-and-down l3 right">
        <?= LastPublicationsWidget::widget(['count' => 6]); ?>
        <p> </p>
        <?= RandomQuoteWidget::widget(); ?>

        <p>&nbsp;</p>
        <?= PopularTagsWidget::widget([]); ?>
    </div>
    <div class="col l9 m12">
            <div class="card post">

                <?php if (!empty($post->image)): ?>
                    <div class="card-image valign-wrapper">
                        <?= Html::img($post->image); ?>
                        <span class="card-title">
                    <span><?= $post->title; ?></span>
                    <small>Опубликовано <a href="#">admin</a> <?= date('H:i', $post->updated_at); ?></small>
                </span>
                    </div>
                <?php endif; ?>
                <div class="card-content">
                    <?php if(empty($post->image)): ?>
                        <span class="card-title">
                    <span><?= $post->title; ?></span>
                    <small>Опубликовано <a href="#">admin</a> <?= date('H:i', $post->updated_at); ?></small>
                </span>
                    <?php endif; ?>
                    <?= $post->content; ?>
                </div>
                <hr>
                <div class="card-content">
                    <?php foreach($post->tags as $tag): ?>
                        <a href="<?= Url::to(['blog/tag', 'tag' => $tag->name]); ?>" class="chip">
                            <?= $tag->name; ?>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
    </div>
</div>