<?php
/**
 * Набор ресурсов для MaterializeCss
 */
namespace app\materialize;

use yii\web\AssetBundle;

/**
 * Стили и скрипты для использования materialize-css
 *
 * Class MaterializeAsset
 * @package app\materialize
 */
class MaterializeAsset extends AssetBundle
{
    public $basePath = '@webroot/lib/materialize';
    public $baseUrl = '@web/lib/materialize';
    public $css = [
        YII_ENV_DEV ? 'css/materialize.css' : 'css/materialize.min.css',
    ];
    public $js = [
        YII_ENV_DEV ? 'js/materialize.js' : 'оы/materialize.min.js',
        'js/init.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}